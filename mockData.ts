// import faker from 'faker'
import { fakeUser } from './src/meta/models/gameUser'

export const mockedData = {
  wallet: [
    {
      gameUserId: 1,
      coins: 0,
      tickets: 0
    }
  ],
  gameUser: [
    fakeUser()
  ]
}