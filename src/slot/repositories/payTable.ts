import { injectable, inject } from 'inversify'
import { PayTableItem } from './../models/payTableData'
import { MongoDb } from './../../services/db'

@injectable()
export class PayTableRepository {
  constructor(@inject(MongoDb) private readonly mongoDb: MongoDb) { }

  async getPayTableData(): Promise<PayTableItem[]> {
    const db = await this.mongoDb.getDb()
    const rows = await db.collection('payTable').aggregate([
      {
        $lookup:
        {
          from: "paymentType",
          localField: "paymentTypeId",
          foreignField: "_id",
          as: "paymentTypes",
        },
      },
      {
        $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ["$paymentTypes", 0] }, "$$ROOT"] } }
      },
      { $project: { paymentTypes: 0 } }

    ]).toArray() as PayTableItem[]
    return rows
  }
}
