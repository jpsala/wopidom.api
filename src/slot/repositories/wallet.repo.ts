import { inject, injectable } from 'inversify'
import { MongoDb } from "../../services/db"
import { Wallet } from './../../meta/models/wallet'

@injectable()
export class WalletRepository {
  constructor(@inject(MongoDb) private mongoDb: MongoDb) { }

  async getWallets(): Promise<Wallet[]> {
    const db = (await this.mongoDb.getDb())
    const wallets = await db.collection('wallet').find().toArray()
    return wallets as Wallet[]
  }
}
