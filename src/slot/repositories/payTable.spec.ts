import { Db } from 'mongodb'
import container from "../../config/container"
import { MongoDb } from '../../services/db'
import { PayTableItem } from '../models/payTableData'
import { PayTableRepository } from './payTable'


let db: Db
const payTableRepository = container.get<PayTableRepository>(PayTableRepository)
let payTableData: PayTableItem[]
const mongoDb = container.get<MongoDb>(MongoDb)

describe('PayTable', () => {
  it('create paytable in db', async () => {
    db = await mongoDb.getDb()
    await db.collection('payTable').deleteMany({})
    const respInsert = await db.collection('payTable').insertMany([
      {
        "paymentTypeId": 545,
        "amount": 3,
        "probability": 0.00,
        "points": 1000
      },
      {
        "paymentTypeId": 555,
        "amount": 3,
        "probability": 0.50,
        "points": 15
      },
      {
        "paymentTypeId": 541,
        "amount": 3,
        "probability": 1.00,
        "points": 100
      },
      {
        "paymentTypeId": 540,
        "amount": 3,
        "probability": 1.50,
        "points": 90
      },
      {
        "paymentTypeId": 537,
        "amount": 3,
        "probability": 2.00,
        "points": 80
      },
      {
        "paymentTypeId": 538,
        "amount": 3,
        "probability": 3.00,
        "points": 70
      },
      {
        "paymentTypeId": 544,
        "amount": 3,
        "probability": 5.00,
        "points": 60
      },
      {
        "paymentTypeId": 554,
        "amount": 3,
        "probability": 8.00,
        "points": 50
      },
      {
        "paymentTypeId": 541,
        "amount": 2,
        "probability": 13.00,
        "points": 40
      },
      {
        "paymentTypeId": 541,
        "amount": 1,
        "probability": 19.00,
        "points": 30
      },
      {
        "paymentTypeId": 540,
        "amount": 2,
        "probability": 22.00,
        "points": 20
      },
      {
        "paymentTypeId": 540,
        "amount": 1,
        "probability": 25.00,
        "points": 10
      }
    ])
    expect(respInsert).toBeObject()
  })
  it('getPayTableData', async () => {
    payTableData = await payTableRepository.getPayTableData()
    // console.log('data', payTableData)
    expect(payTableData).toBeArray()
  })
})
/*
const payTable = await getPayTable()
 const conn = await getSlotConnection()
try {
  [payTable] = await conn.query(`
    select s.paymentType, pt.symbol_amount, pt.probability, pt.points
      from pay_tablApt
    inner join symbol s on s.id = pt.symbol_id
    order by pt.probability asc`)
return payTable
} finally {
await conn.destroy()
}
*/


