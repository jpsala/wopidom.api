import createError from 'http-errors'
import { Request, Response } from "express"
import { injectable, inject } from 'inversify'
import statusCodes from "http-status-codes"
import { SettingService } from "../../services/setting"
import { SpinService } from '../services/spin'
import { WalletService } from "../services/wallet.service"
import { ProfileService } from "../services/profile"
import { ReqUser } from './../../models/reqUser'

@injectable()
export class SlotController {
  constructor(
    @inject(WalletService) private readonly walletService: WalletService,
    @inject(SettingService) private readonly settingService: SettingService,
    @inject(SpinService) private readonly spinService: SpinService,
    @inject(ProfileService) private readonly profileService: ProfileService
  ) { }

  async walletGet(req: Request, res: Response): Promise<any> {
    const resp = await this.walletService.getWallets()
    res.status(200).json(resp)
  }

  async spinGet(req: Request, res: Response): Promise<any> {
    const { multiplier, user } = await this.getSpinParameters(req)
    const resp = await this.spinService.getSpinData(multiplier, user)
    res.status(200).json(resp)
  }

  async profileGet(req: Request, res: Response): Promise<void> {
    const { user } = await this.getSpinParameters(req)
    const resp = await this.profileService.getProfile(true, user)
    res.status(200).json(resp)
  }

  private async getSpinParameters(req: Request) {
    const multiplier = Number(req.query.multiplier)
    if (multiplier < 1) throw createError(statusCodes.BAD_REQUEST, 'multiplayer must be > 0')
    if (!multiplier) throw createError(statusCodes.BAD_REQUEST, "multiplier is required")
    const maxMultiplier = Number(await this.settingService.getSetting('maxMultiplier', '1'))
    if (multiplier > maxMultiplier) throw createError(createError(statusCodes.BAD_REQUEST, `multiplayer (${multiplier}) is bigger than maxMultiplier setting (${maxMultiplier})`))
    const user: ReqUser = req.user as ReqUser
    return { multiplier, user }
  }

  postmanGet(req: Request, res: Response): any {
    res.status(200).json(req.body)
  }
}
