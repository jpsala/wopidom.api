import statusCodes from 'http-status-codes'
import createError from 'http-errors'
import { inject, injectable } from 'inversify'
import { AuthMiddleware } from "../../middleware/authMiddleware"
import { MongoDb } from "../../services/db"
import { GameUserRepository } from "../../meta/repositories/gameUser.repo"
import { GameUser } from "../../meta/models/gameUser"
import { ReqUser } from './../../models/reqUser'

@injectable()
export class ProfileService {
  constructor(
    @inject(GameUserRepository) private readonly gameUserRepository: GameUserRepository,
    @inject(MongoDb) private readonly mongoDb: MongoDb,
    @inject(AuthMiddleware) private readonly authMiddleware: AuthMiddleware
  ) { }

  async getProfile(partial = false, reqUser: ReqUser): Promise<Partial<GameUser>> {
    if (!reqUser.userId) throw new Error('userId udefined!')
    const user: GameUser = await this.gameUserRepository.getGameUser(reqUser.userId) as GameUser
    if (!user) throw createError(statusCodes.BAD_REQUEST, 'No user in session')
    if (partial)
      return { firstName: user.firstName, lastName: user.lastName, email: user.email }
    return user as Partial<GameUser>
  }

}