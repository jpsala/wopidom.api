import { injectable, inject } from 'inversify'
import { PayTableRepository } from './../repositories/payTable'
import { PayTableItem } from './../models/payTableData'

@injectable()
export class PayTableService {
  constructor(@inject(PayTableRepository) private readonly payTableRepository: PayTableRepository) { }
  async getPayTableData(): Promise<PayTableItem[]> {
    return await this.payTableRepository.getPayTableData()
  }
}