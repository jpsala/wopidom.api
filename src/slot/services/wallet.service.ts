import { inject, injectable } from 'inversify'
import { WalletRepository } from "../repositories/wallet.repo"
// import 'adata'

@injectable()
export class WalletService {
  constructor(@inject(WalletRepository) private readonly walletRepository: WalletRepository) { }
  async getWallets(): Promise<any> {
    const wallets = await this.walletRepository.getWallets()
    return Promise.resolve(wallets)
  }
}
