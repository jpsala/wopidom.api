import createError from 'http-errors'
import statusCodes from 'http-status-codes'
import { injectable, inject } from 'inversify'
// import { plainToClass } from "class-transformer"
import { getRandomNumber } from '../../../helpers'
import { GameUser } from "../../../meta/models/gameUser"
import { AuthMiddleware } from "../../../middleware/authMiddleware"
import { GameUserRepository } from "../../../meta/repositories/gameUser.repo"
import { SettingService } from '../../../services/setting'
import { PayTableService } from "../payTable"
import { ReqUser } from '../../../models'
import { WinPaymentTypeEnum } from '../../models/paymentTypeEnum'
import { SymbolsDataDTO } from './../../models/spinData'
import { SymbolsData, PayTableItem, SpinData, PaymentType, WinData } from './../../models'
// let idx = 0
@injectable()
export class SpinService {
  constructor(
    @inject(AuthMiddleware) private readonly authMiddleware: AuthMiddleware,
    @inject(SettingService) private readonly settingService: SettingService,
    @inject(GameUserRepository) private readonly gameUserRepository: GameUserRepository,
    @inject(PayTableService) private readonly payTable: PayTableService
  ) { }

  async getSpinData(multiplier: number, reqUser: ReqUser): Promise<SpinData> {
    // console.log('idx', ++idx)
    await this.checkParams(multiplier)
    const user: GameUser = await this.gameUserRepository.getGameUser(reqUser.userId) as GameUser
    const bet: number = await this.calcAndGetBet(multiplier)
    if (!hasCredit(bet, user.wallet.coins)) throw createError(statusCodes.BAD_REQUEST, 'No Credit!')
    // @TODO Save spin to db
    let isWin = checkWithRandomIfWins()
    const payTable = await this.payTable.getPayTableData()
    const spinCount = Number(await this.settingService.getSetting('spinCount', 0)) + 1
    const spinsForJackpot = Number(await this.settingService.getSetting('spinsForJackpot', 10))
    await this.settingService.setSetting('spinCount', spinCount)

    //lógica spin
    const isJackpot = spinCount === spinsForJackpot
    const filltable = this.getFillTable(payTable)
    let winPaymentTypeName: WinPaymentTypeEnum
    let winPaytableItem: PayTableItem

    if (isJackpot) {
      winPaytableItem = this.getJackpotItem(payTable)
      // winPaymentTypeItems = [this.getJackpotItem(payTable)]
      isWin = true
      multiplier = 1
      winPaymentTypeName = 'jackpot'
    } else if (isWin) {
      winPaytableItem = this.getWinItem(payTable)
      // winPaymentTypeItems = [winPaytableItem]
      winPaymentTypeName = winPaytableItem.paymentTypeName === 'ticket' ? 'ticket' : 'coin'
      if (winPaymentTypeName === 'ticket') multiplier = 1
    } else {
      winPaytableItem = { paymentTypeName: '', amount: 0, points: 0, probability: 0 }
      winPaymentTypeName = 'coin'
    }
    const symbolsData = this.getWinRowWithEmptyFilled(winPaytableItem, filltable)

    const winData: WinData = { type: winPaymentTypeName, amount: 1 }
    const symbolsDataDTO: SymbolsDataDTO = [
      { paymentType: symbolsData[0]?.paymentTypeName as string },
      { paymentType: symbolsData[1]?.paymentTypeName as string },
      { paymentType: symbolsData[2]?.paymentTypeName as string }
    ]
    return { wallet: user.wallet, isWin, symbolsData: symbolsDataDTO, winData: isWin ? winData : {} as WinData }
    /*
    export interface SpinData {
      >symbolsData: PaymentType[];
      >isWin: boolean;
      >wallet: Wallet;
      winData: WinData
    }
    {
      _id?: string;
      name: string;
      textureUrl?: string;
    }
    */
  }
  getWinRowWithEmptyFilled(winPaytableItem: PayTableItem, fillTable: PayTableItem[]): SymbolsData {
    const winSymbolAmount = winPaytableItem.amount
    const winSymbolPaymentType = winPaytableItem.paymentTypeName
    const filledSymbolRowToReturn: SymbolsData = []
    for (let idx = 0; idx < winSymbolAmount; idx++)
      filledSymbolRowToReturn.push({ paymentTypeName: winSymbolPaymentType, isPaying: true })

    const symbolsAmountToFill = 3 - winSymbolAmount
    for (let idx = 0; idx < symbolsAmountToFill; idx++) {
      // const symbolRowsForFilling = getSymbolRowsForFilling(fillTable)
      const symbolRowForFilling = this.getPaymentTypeForFilling(fillTable, filledSymbolRowToReturn)
      filledSymbolRowToReturn.push({ paymentTypeName: symbolRowForFilling.name, isPaying: false })
    }
    return filledSymbolRowToReturn
  }
  getPaymentTypeForFilling(payTableItemsForFilling: PayTableItem[], allreadyFilledSymbols: SymbolsData): PaymentType {
    const payTableItem = payTableItemsForFilling.filter((fillingSymbolRow) => {
      // console.log('allreadyFilledSymbols.length', allreadyFilledSymbols.length)
      if (allreadyFilledSymbols.length === 0)
        // console.log('fillingS', fillingSymbolRow)
        return fillingSymbolRow


      const isInValid = allreadyFilledSymbols.find((afSymbol) => {
        const found = (afSymbol?.paymentTypeName === fillingSymbolRow.paymentTypeName)
        // if (!encontrado)
        //   console.log('symbol', fillingSymbolRow.payment_type)
        return found
      })
      return !isInValid
    })

    const randomNumber = getRandomNumber(0, payTableItem.length - 1)

    return {
      name: payTableItem[randomNumber].paymentTypeName
    }
  }
  getWinItem(payTable: PayTableItem[]): PayTableItem {
    const randomNumber = getRandomNumber(1, 100)
    let floor = 0
    const winRow: PayTableItem = <PayTableItem>payTable.find((row) => {
      const isWin = ((randomNumber > floor) && (randomNumber <= floor + Number(row.probability)))
      floor += Number(row.probability)
      return isWin
    })
    return winRow
  }
  getFillTable(payTable: PayTableItem[]): PayTableItem[] {
    const rowsWith3 = payTable.filter((payTableRow) => payTableRow.amount === 3)
    const rowsWithLessThan3 = payTable.filter((payTableRow) => payTableRow.amount < 3)
    return rowsWith3.filter((rowWith3) => {
      const isInOtherRow = rowsWithLessThan3.find((rowWithLessThan3) => {
        const found = (rowWithLessThan3.paymentTypeName === rowWith3.paymentTypeName)
        return found
      })
      return !isInOtherRow
    })
  }
  getJackpotItem(payTable: PayTableItem[]): PayTableItem {
    return payTable[0]
  }
  async calcAndGetBet(multiplier: number): Promise<number> {
    const betPrice = await this.settingService.getSetting('betPrice', 1)
    if (typeof betPrice !== 'number') throw createError(statusCodes.BAD_REQUEST, 'checkCredit, betPrice has an invalid type')
    return betPrice * multiplier
  }
  async checkParams(multiplier: number): Promise<void> {
    const maxMultiplier = await this.settingService.getSetting('maxMultiplier', 1)
    if (multiplier > maxMultiplier)
      throw createError(
        statusCodes.BAD_REQUEST,
        `Multiplier param is ${multiplier} and the top is ${maxMultiplier}`
      )
  }
}
const hasCredit = (bet: number, coins: number) => {
  const coinsInTheWallet = coins || 0
  return (bet <= coinsInTheWallet)
}
const checkWithRandomIfWins = () => getRandomNumber() > 20






