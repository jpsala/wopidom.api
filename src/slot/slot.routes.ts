import { inject, injectable } from 'inversify'
import { Router } from 'express'
import { MetaController } from "../meta/meta.controller"
import { AuthMiddleware } from '../middleware/authMiddleware'
import { SlotController } from './controller'

@injectable()
export class SlotRouter {
  public router = Router();
  private checkToken: any;
  constructor(
    @inject(AuthMiddleware) private readonly authMiddleware: AuthMiddleware,
    @inject(MetaController) private readonly metaController: MetaController,
    @inject(SlotController) private readonly slotController: SlotController
  ) {
    this.checkToken = this.authMiddleware.checkToken.bind(this.authMiddleware)
    this.initializeRoutes()
  }

  private initializeRoutes() {
    this.router.route('/wallet').get(this.checkToken, this.slotController.walletGet.bind(this.slotController))
    this.router.route('/spin').get(
      this.authMiddleware.checkToken.bind(this.authMiddleware),
      this.slotController.spinGet.bind(this.slotController))
    this.router.route('/profile').get(
      this.authMiddleware.checkToken.bind(this.authMiddleware),
      this.slotController.profileGet.bind(this.slotController))
    this.router.route('/postman').get(
      this.slotController.postmanGet.bind(this.slotController))
  }
}
