export interface SymbolData {
  paymentTypeName: string;
  isPaying: boolean;
}
export type SymbolsData = [SymbolData?, SymbolData?, SymbolData?]
