export interface PaymentType {
  _id?: string;
  name: string;
  textureUrl?: string;
}