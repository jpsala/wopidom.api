export interface PayTableItem {
  paymentTypeName: string;
  amount: number;
  probability: number;
  points: number;
}