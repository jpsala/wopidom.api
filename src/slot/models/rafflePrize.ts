import { Localization } from './../../meta/models/localization'
export interface RafflePrizeData {
  id: number;
  closingDate: Date;
  textureUrl: string;
  raffleNumberPrice: number;
  itemHighlight: boolean;
  name?: string;
  description?: string;
  localizationData: Localization[];
}
