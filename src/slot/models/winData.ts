import { WinPaymentTypeEnum } from './paymentTypeEnum'

export interface WinData {
  type: WinPaymentTypeEnum;
  amount: number;
}