import { GameUser } from './../../meta/models/gameUser'

export interface RafflePurchase {
  id: number;
  gameUser: GameUser;
  transactionDate: Date;
  tickets: number;
  closingDate: Date;
  raffleNumbers: number;
  raffleItemId: number;
}
