import { Wallet } from './../../meta/models/wallet'
import { WinData } from '.'
export type SymbolsDataDTO = [{ paymentType: string }, { paymentType: string }, { paymentType: string }]

export interface SpinData {
  symbolsData: SymbolsDataDTO;
  isWin: boolean;
  wallet: Wallet;
  winData: WinData
}