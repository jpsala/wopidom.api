import { isUndefined, isNumber } from 'util'
import createError from 'http-errors'
import statusCodes from 'http-status-codes'
import { inject, injectable } from 'inversify'
import { SettingRepo } from "../repositories/setting"

@injectable()
export class SettingService {
  constructor(
    @inject(SettingRepo) private settingRepo: SettingRepo
  ) { }

  async setSetting(key: string, value: string | number | undefined = undefined): Promise<void> {
    await this.settingRepo.setSetting(key, value)
  }

  async getSetting(key: string, defaultValue?: number | string): Promise<number | string> {
    const value = await this.settingRepo.getSetting(key, defaultValue as string | undefined)
    if (typeof defaultValue === 'number') {
      const numValue = isUndefined(value) ? 0 : Number(value)
      if (isNaN(numValue) || !isNumber(numValue))
        throw createError(statusCodes.INTERNAL_SERVER_ERROR,
          `getSetting: bad value for ${key}(${value}), should be of type ${typeof defaultValue} but stored value is of type ${typeof value}`)
      return numValue
    }
    return String(value)
  }
}