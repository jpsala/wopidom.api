import { inject, injectable } from "inversify"
import { MongoDb } from './db'

@injectable()
export class Init {
  constructor(
    @inject(MongoDb) private readonly mongoDb: MongoDb
  ) { }

  public async run(): Promise<void> {
    const db = await this.mongoDb.getDb()
    await db.collection('setting').createIndex({ 'key': 1 }, { unique: true })
  }
}