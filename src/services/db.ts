import createError from 'http-errors'
import { MongoClient, Db } from "mongodb"
import { injectable, } from 'inversify'
import { getOption } from '../config'
import 'reflect-metadata'

const client: MongoClient[] = []
@injectable()
export class MongoDb {
  private database: string | undefined = undefined;
  public close(): void {
    if (this.database !== undefined && client[this.database] !== undefined) {
      client[this.database].close()
      delete client[this.database]
    }
  }

  public async getDb(): Promise<Db> {
    const url = 'mongodb://localhost:27017'
    this.database = getOption('database')
    if (this.database === undefined) throw createError(500, 'database is undefined')
    if (!client[this.database])
      client[this.database] = await MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })

    return client[this.database].db(this.database) as Db
  }
}