import { Router } from 'express'
import container from "./config/container"
import { SlotRouter } from './slot/slot.routes'

// import { MetaRouter } from './meta/meta.routes'
const router = Router()
// const slotRouter = new SlotRouter()
const slotRouter = container.get(SlotRouter)
// const metaRouter = new MetaRouter()
router.use('/slot', slotRouter.router)
export default router
