le agregué unos campos a la clase del happy hour, te la tiro acá, después si querés hace un ticket,

public class EventData
    {
        public string eventType;
        public string description;
        public bool isActive;
        public int secondsToStart;
        public int secondsToEnd;
        public bool devOnly;
        public string textureUrl;
    }

los ultimos dos, uno que van a pedir/necesitar que el evento lo vean solo en modo dev, que es una bandera interna del cliente,

y un texture url, que por ahora es para el icono de la notificacion, pero puede que haya mas texturas si presenta algun popup

cuando esté más estabilizado todo, vamos a necesitar una clausula en el server, un whitelist para los ids de las cuentas dev