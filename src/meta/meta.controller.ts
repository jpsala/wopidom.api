import createError from 'http-errors'
import { inject, injectable } from 'inversify'
import { Request, Response } from 'express'
import { GameUserService } from './services/gameUser.service'

@injectable()
export class MetaController {
  constructor(@inject(GameUserService) private readonly gameUserService: GameUserService) { }

  async gameUserGet(req: Request, res: Response): Promise<any> {
    if (req.query.id === undefined) throw createError(createError.BadRequest, 'Parameter id is required')
    const resp = await this.gameUserService.getGameUser(req.query.id as string)
    res.status(200).json(resp)
  }
}
