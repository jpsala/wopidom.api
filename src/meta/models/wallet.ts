export interface Wallet {
  tickets: number;
  coins: number;
}
