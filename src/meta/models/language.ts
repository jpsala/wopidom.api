export interface Language {
  id: number;
  languageCode: string;
  textureUrl: string;
  localizationUrl: string;
}
