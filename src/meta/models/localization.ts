export interface Localization {
  id: number;
  raffleId: number;
  languageCode: string;
  name: string;
  description: string;
}
