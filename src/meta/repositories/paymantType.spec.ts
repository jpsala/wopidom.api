import { Db } from 'mongodb'
import container from "../../config/container"
import { MongoDb } from '../../services/db'
import { PaymentType } from './../../slot/models/paymentType'
import { PaymentTypeRepository } from './paymentType'


let db: Db
let paymentTypes: PaymentType[]
const paymentTypeRepository = container.get<PaymentTypeRepository>(PaymentTypeRepository)
const mongoDb = container.get<MongoDb>(MongoDb)

describe('paymentTypes', () => {
  it('Insert paymentTypes in db', async () => {
    db = await mongoDb.getDb()
    await db.collection('paymentType').deleteMany({})
    const respInsert = await db.collection('paymentType').insertMany([
      {
        "_id": 533,
        "paymentType": "apple",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/apple.png"
      },
      {
        "_id": 534,
        "paymentType": "avocado",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/avocado.png"
      },
      {
        "_id": 535,
        "paymentType": "banana",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/banana.png"
      },
      {
        "_id": 536,
        "paymentType": "blackberry",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/blackberry.png"
      },
      {
        "_id": 537,
        "paymentType": "blueberry",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/blueberry.png"
      },
      {
        "_id": 538,
        "paymentType": "cherry",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/cherry.png"
      },
      {
        "_id": 539,
        "paymentType": "coconut",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/coconut.png"
      },
      {
        "_id": 540,
        "paymentType": "coin",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/coin.png"
      },
      {
        "_id": 541,
        "paymentType": "coinbag",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/coinbag.png"
      },
      {
        "_id": 542,
        "paymentType": "dragonfruit",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/dragonfruit.png"
      },
      {
        "_id": 543,
        "paymentType": "fig",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/fig.png"
      },
      {
        "_id": 544,
        "paymentType": "grape",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/grape.png"
      },
      {
        "_id": 545,
        "paymentType": "jackpot",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/jackpot.png"
      },
      {
        "_id": 546,
        "paymentType": "kiwi",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/kiwi.png"
      },
      {
        "_id": 547,
        "paymentType": "lychee",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/lychee.png"
      },
      {
        "_id": 548,
        "paymentType": "orange",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/orange.png"
      },
      {
        "_id": 549,
        "paymentType": "papaya",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/papaya.png"
      },
      {
        "_id": 550,
        "paymentType": "pear",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/pear.png"
      },
      {
        "_id": 551,
        "paymentType": "pineapple",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/pineapple.png"
      },
      {
        "_id": 552,
        "paymentType": "pomegranate",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/pomegranate.png"
      },
      {
        "_id": 553,
        "paymentType": "seven",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/seven.png"
      },
      {
        "_id": 554,
        "paymentType": "strawberry",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/strawberry.png"
      },
      {
        "_id": 555,
        "paymentType": "ticket",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/ticket.png"
      },
      {
        "_id": 556,
        "paymentType": "watermelon",
        "textureUrl": "http://wopidom.homelinux.com/public/assets/symbols/live/watermelon.png"
      }
    ])
    expect(respInsert).toBeObject()
  })
  it('getPayTableData', async () => {
    paymentTypes = await paymentTypeRepository.getPaymentTypes()
    console.log('data', paymentTypes)
    expect(paymentTypes).toBeArray()
  })
})



