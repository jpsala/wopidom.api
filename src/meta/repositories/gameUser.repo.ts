import createError from 'http-errors'
// import createError from 'http-errors'
import { injectable, inject } from 'inversify'
import { MongoDb } from "../../services/db"
import { GameUser, createGameUser } from "../models/gameUser"



@injectable()
export class GameUserRepository {
  constructor(@inject(MongoDb) private mongoDb: MongoDb) { }

  async getGameUser(userId: string): Promise<GameUser | null> {
    const db = await this.mongoDb.getDb()
    const user: GameUser | null = await db.collection('gameUser').findOne({ _id: userId })
    return user
  }

  async getGameUserByDeviceId(
    deviceId: string,
    options: { create?: false } = {}): Promise<GameUser> {
    const db = await this.mongoDb.getDb()
    let user = (await db.collection('gameUser').findOne({ deviceId })) as GameUser
    if (user === null && options.create) {
      user = createGameUser({
        deviceId, isNew: true, wallet: { coins: 0, tickets: 0 }
      })
      const { result, ops } = await db.collection('gameUser').insertOne(user)
      user = ops[0]
      if (result.ok !== 1) throw createError(500, `Couldn't insert user for deviceId ${deviceId}`)
    }
    return user
  }
}
