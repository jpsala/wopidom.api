import { injectable, inject } from 'inversify'
import { MongoDb } from '../../services/db'
import { PaymentType } from './../../slot/models/paymentType'

@injectable()
export class PaymentTypeRepository {
  constructor(@inject(MongoDb) private mongoDb: MongoDb) { }

  async getPaymentTypes(): Promise<PaymentType[]> {
    const db = await this.mongoDb.getDb()
    const paymentTypes: PaymentType[] = await db.collection('paymentType').find({}).toArray()
    return paymentTypes
  }
}