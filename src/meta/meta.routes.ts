import { Router } from 'express'
import { inject } from 'inversify'
import { AuthMiddleware } from '../middleware/authMiddleware'
import { MetaController } from './meta.controller'

export class MetaRouter {
  public router = Router();
  private checkToken: any;
  constructor(
    @inject(AuthMiddleware) private readonly authMiddleware: AuthMiddleware,
    @inject(MetaController) private readonly metaController: MetaController
  ) {
    this.checkToken = this.authMiddleware.checkToken.bind(this.authMiddleware)
    this.initializeRoutes()
  }

  private initializeRoutes() {
    this.router
      .route('/game-user')
      .get(this.checkToken, this.metaController.gameUserGet.bind(this.metaController))
  }
}
