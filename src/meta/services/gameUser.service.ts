import { inject, injectable } from 'inversify'
import { GameUser } from '../models/gameUser'
import { GameUserRepository } from '../repositories/gameUser.repo'


@injectable()
export class GameUserService {
  constructor(
    @inject(GameUserRepository) private readonly gameUserRepository: GameUserRepository
  ) { }

  async getGameUser(userId: string): Promise<GameUser> {
    const user = await this.gameUserRepository.getGameUser(userId) as GameUser
    return user
  }

  async getProfile(userId: string): Promise<Partial<GameUser>> {
    const user = await this.getGameUser(userId)
    return user
  }
}