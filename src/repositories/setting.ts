import { inject, injectable } from 'inversify'
import { MongoDb } from '../services/db'

@injectable()
export class SettingRepo {
  constructor(
    @inject(MongoDb) private readonly mongoDb: MongoDb
  ) { }

  async setSetting(key: string, value: string | number | undefined = undefined): Promise<void> {
    const db = await this.mongoDb.getDb()
    // const setting = await db.collection('setting').findOne({ key })
    await db.collection('setting').replaceOne(
      { key },
      { key, value },
      { upsert: true }
    )
  }


  async getSetting(key: string, defaultValue: string | undefined = undefined): Promise<string> {
    const db = await this.mongoDb.getDb()
    const setting = await db.collection('setting').findOne({ key })
    if (setting == null && defaultValue != null) {
      this.setSetting(key, defaultValue)
      return defaultValue as string
    }
    return setting?.value
  }
}