// import { makeLoggerMiddleware } from 'inversify-logger-middleware'
import 'reflect-metadata'
import { Container } from 'inversify'

// export const logger = makeLoggerMiddleware()
const container = new Container({ autoBindInjectable: true })
// container.applyMiddleware(logger)
// container.bind<WalletRepository>(WalletRepository).to(WalletRepository)

export default container