/* eslint-disable no-process-env */
import config from "./configData"

export const getOption = (option: string, defaultValue: string | undefined = undefined): any => {
  const environment = process.env.NODE_ENV || 'default'
  const value = config[environment][option]
  // console.log('config', option, value)
  return value ?? defaultValue
}