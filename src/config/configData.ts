const configData = {
  "default": {
    "database": "wopidom"
  },
  "development": {},
  "testing": {
    "database": "wopiTest"
  },
  "staging": {},
  "production": {}
}
export default configData