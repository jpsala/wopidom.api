import { Request, Response, NextFunction } from 'express'

export class Fakexpress {
  res: Partial<Response> = {
    statusCode: 200,
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    status: jest.fn().mockImplementation((code) => {
      this.res.statusCode = code
      // this.res.send = message => ({code, message})
      return this.res
    }),
    json: jest.fn().mockImplementation((param) => {
      this.responseData = param
      return this.res
    }),
    cookie: jest.fn(),
    clearCookie: jest.fn()
  };

  req: Partial<Request>;
  next: Partial<NextFunction> = jest.fn();
  responseData: any;
  constructor(req: Partial<Request>) {
    this.req = req
    if (!this.req?.headers) this.req.headers = {}
    this.req.headers['dev-request'] = 'true'
  }
}
