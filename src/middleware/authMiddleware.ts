import statusCodes from 'http-status-codes'
import { NextFunction, Request, Response } from 'express'
import createError from 'http-errors'
import { injectable, inject } from 'inversify'
import { JwtService } from "../services/jwtService"
import { GameUser } from "../meta/models/gameUser"
import { GameUserRepository } from "../meta/repositories/gameUser.repo"
import { ReqUser } from './../models/reqUser'


@injectable()
export class AuthMiddleware {
  private savedUser: GameUser | undefined;
  private readonly jwtService: JwtService;
  private user: GameUser | undefined = undefined;
  constructor(@inject(GameUserRepository) private readonly gameUserRepository: GameUserRepository) {
    this.jwtService = new JwtService()
  }

  public async checkToken(req: Request, res: Response, next: NextFunction): Promise<any> {
    const { 'dev-request': dev } = req.headers
    const isDev = (dev === 'true')
    if (isDev) {
      let deviceId = String(req.query.deviceId)
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      if (!deviceId) deviceId = req.body?.deviceId as string
      if (deviceId == null)
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        throw createError(createError.BadRequest, `deviceId parameter missing ${req.baseUrl}${req.route}`)
      const user = await this.gameUserRepository.getGameUserByDeviceId(deviceId)
      if (user == null) throw createError(statusCodes.BAD_REQUEST, 'DeviceId is not stored')
      req.user = { userId: user._id, deviceId: user.deviceId }
      return next()
    }
    const { sessionToken } = req.query
    const { decodedToken, error } = <{ decodedToken: ReqUser, error: Error }>this.jwtService.verifyToken(sessionToken as string)
    if (error || !decodedToken.userId) {
      const message: string = error ? error.message : 'There is not user registered with this token'
      console.log(`checkToken: ${message}`, req.baseUrl, sessionToken)
      return res.status(401).send({ auth: false, message })
    }
    if (decodedToken.deviceId == null) throw createError(statusCodes.BAD_REQUEST, 'deviceId undefined')
    if (decodedToken.userId == null) throw createError(statusCodes.BAD_REQUEST, 'user undefined')
    // await this.setUser(decodedToken.deviceId, decodedToken.id)
    const reqUser: ReqUser = { userId: decodedToken.userId, deviceId: decodedToken.deviceId } as ReqUser
    req.user = reqUser
    return next()
  }
}
