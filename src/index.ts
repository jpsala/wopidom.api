import { Init } from './services/init'
import createApp from './app'
import container from "./config/container"

void (async function main(): Promise<void> {
  const app = createApp()
  const port = 8888
  const name = 'wopidom api'
  const init = container.get<Init>(Init)
  await init.run()
  app.listen(port, () => {
    console.info(`${name} started at port ${port}`)
  })
})()
