mongo wopidom --eval "printjson(db.dropDatabase())"
mongodump -h localhost:27017 --db wopiTest
mongorestore -h localhost:27017 -d wopidom dump/wopiTest
mongo wopidom --eval 'printjson(db.gameUser.insertMany(
    [
  {
    "_id": "borrarrrrr",
    "address": "438 Athena Square",
    "age": "39",
    "city": "Abeside",
    "country": "Saudi Arabia",
    "countryPhoneCode": "BY",
    "createdAt": {"$date": "2019-09-20T14:03:27.223Z"},
    "deviceId": "device1",
    "deviceModel": "fugiat",
    "deviceName": "est",
    "email": "Shanon.Pagac@hotmail.com",
    "firstName": "Ruthie",
    "isMale": true,
    "isNew": false,
    "languageCode": "IO",
    "lastName": "Lehner",
    "modifiedAt": {"$date": "2020-04-26T04:45:17.453Z"},
    "password": "zSu5vmtiWl6Yyye",
    "phoneCode": "earum",
    "phoneNumber": "968.822.5068 x114",
    "state": "Iowa",
    "wallet": {
      "coins": 39,
      "tickets": 32
    },
    "zipCode": "41222-7935"
  }
]
))'